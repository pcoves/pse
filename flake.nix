{
    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
        flake-utils.url = "github:numtide/flake-utils";
        rust-overlay = {
            url = "github:oxalica/rust-overlay";
            inputs = {
                nixpkgs.follows = "nixpkgs";
                flake-utils.follows = "flake-utils";
            };
        };
    };

    outputs = { nixpkgs, flake-utils, rust-overlay, ... }:
        flake-utils.lib.eachDefaultSystem (system:
        let overlays = [(import rust-overlay)];
            pkgs = import nixpkgs { inherit system overlays; };

            nativeBuildInputs = with pkgs; [
                git
                rust-bin.stable.latest.default
            ] ++ (if system == "aarch64-darwin" || system == "x86_64-darwin" then [
                darwin.apple_sdk.frameworks.Security
            ] else [ ]);

            buildInputs = with pkgs; [openssl];

            src = pkgs.lib.cleanSource ./.;

            cargoToml = (pkgs.lib.importTOML ./Cargo.toml).package;
            pname = cargoToml.name;
            version = cargoToml.version;

            default = pkgs.rustPlatform.buildRustPackage {
                inherit pname version src nativeBuildInputs buildInputs;
                cargoLock.lockFile = ./Cargo.lock;
            };

        in with pkgs; {
            devShells.default = mkShell {
                inherit nativeBuildInputs buildInputs;

                shellHook = ''
                    alias find="${fd}/bin/fd"
                    alias grep="${ripgrep}/bin/rg"
                    alias l="${eza}/bin/eza -lAh"
                '';
            };

            packages = {
                inherit default;

                container = dockerTools.buildImage {
                    name = "${pname}";
                    tag = "${version}";

                    config = {
                        Entrypoint = ["${default}/bin/${pname}"];
                    };
                };
            };
        }
    );
}
