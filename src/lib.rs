mod parameter;

use crate::parameter::{Parameter, Parameters};
use aws_sdk_ssm::{
    operation::describe_parameters::DescribeParametersOutput,
    types::builders::ParameterStringFilterBuilder, Client,
};

pub async fn r#match(
    client: &Client,
    pattern: &str,
    with_decryption: bool,
    sort_by_name: bool,
    last_modified_date: bool,
) -> anyhow::Result<()> {
    async fn describe_parameters(
        client: &Client,
        pattern: &str,
        token: Option<String>,
    ) -> anyhow::Result<DescribeParametersOutput> {
        Ok(client
            .describe_parameters()
            .parameter_filters(
                ParameterStringFilterBuilder::default()
                    .key("Name")
                    .values(pattern)
                    .option("Contains")
                    .build()
                    .unwrap(),
            )
            .set_next_token(token)
            .send()
            .await?)
    }

    async fn get_parameters(
        client: &Client,
        describe_parameters_output: &DescribeParametersOutput,
        with_decryption: bool,
    ) -> anyhow::Result<Vec<Parameter>> {
        Ok(client
            .get_parameters()
            .set_names(
                describe_parameters_output
                    .parameters()
                    .iter()
                    .map(|parameter| parameter.arn().map(str::to_string))
                    .collect::<Option<Vec<_>>>(),
            )
            .with_decryption(with_decryption)
            .send()
            .await?
            .parameters()
            .iter()
            .map(|parameter| Parameter {
                r#type: parameter.r#type().cloned().unwrap(),
                name: parameter.name().map(Into::into).unwrap(),
                value: parameter.value().map(Into::into).unwrap(),
                last_modified_date: parameter.last_modified_date().cloned().unwrap(),
            })
            .collect::<Vec<_>>())
    }

    let mut describe_parameters_output = describe_parameters(client, pattern, None).await?;
    let mut parameters = get_parameters(client, &describe_parameters_output, with_decryption)
        .await
        .unwrap_or_else(|_| vec![]);

    while let Some(token) = describe_parameters_output.next_token() {
        describe_parameters_output =
            describe_parameters(client, pattern, Some(token.to_string())).await?;
        parameters.append(
            &mut get_parameters(client, &describe_parameters_output, with_decryption)
                .await
                .unwrap_or_else(|_| vec![]),
        );
    }

    print!(
        "{}",
        Parameters::new(
            parameters,
            with_decryption,
            sort_by_name,
            last_modified_date
        )
    );

    Ok(())
}
