pub use clap::Parser;

#[derive(Debug, Parser)]
pub struct Options {
    #[clap(short, long, env = "AWS_PROFILE")]
    pub profile: String,

    #[clap(short, long, env = "AWS_REGION")]
    pub region: String,

    #[clap(
        short = 'd',
        long = "decrypt",
        env = "WITH_DECRYPTION",
        help = "Show decrypted secrets"
    )]
    pub with_decryption: bool,

    #[clap(
        short = 's',
        long = "sort",
        env = "SORT_BY_NAME",
        help = "Sort entries by path"
    )]
    pub sort_by_name: bool,

    #[clap(
        short,
        long,
        env = "LAST_MODIFIED_DATE",
        help = "Show last modified date"
    )]
    pub last_modified_date: bool,

    pub pattern: String,
}
