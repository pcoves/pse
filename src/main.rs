mod options;

use crate::options::{Options, Parser};
use aws_config::load_from_env;
use aws_sdk_ssm::Client;
use pse::r#match;
use std::env::set_var;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv().ok();

    let options = Options::parse();

    set_var("AWS_PROFILE", options.profile);
    set_var("AWS_REGION", options.region);

    let client = Client::new(&load_from_env().await);

    r#match(
        &client,
        &options.pattern,
        options.with_decryption,
        options.sort_by_name,
        options.last_modified_date,
    )
    .await?;

    Ok(())
}
