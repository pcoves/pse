use aws_sdk_ssm::{primitives::DateTime, types::ParameterType};
use std::fmt::Display;

#[derive(Debug)]
pub struct Parameter {
    pub r#type: ParameterType,
    pub name: String,
    pub value: String,
    pub last_modified_date: DateTime,
}

pub struct Parameters {
    parameters: Vec<Parameter>,
    last_modified_date: bool,
}

impl Parameters {
    pub fn new(
        mut parameters: Vec<Parameter>,
        with_decryption: bool,
        sort: bool,
        last_modified_date: bool,
    ) -> Self {
        if sort {
            parameters.sort_by(|lhs, rhs| lhs.name.cmp(&rhs.name));
        }

        if !with_decryption {
            parameters = parameters
                .into_iter()
                .map(|mut parameter| {
                    if parameter.r#type == ParameterType::SecureString {
                        parameter.value = "**********".into()
                    }
                    parameter
                })
                .collect();
        }

        Self {
            parameters,
            last_modified_date,
        }
    }
}

impl Display for Parameters {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        use colored::Colorize;

        let (mut name, mut value) = (0, 0);

        for parameter in &self.parameters {
            name = name.max(parameter.name.len());
            if parameter.r#type != ParameterType::SecureString {
                value = value.max({
                    let mut value = 0;
                    for line in parameter.value.lines() {
                        value = value.max(line.len());
                    }
                    value
                });
            }
        }

        value = value.max(10);

        for parameter in &self.parameters {
            write!(f, "| {:name$}", parameter.name.green())?;
            if self.last_modified_date {
                write!(
                    f,
                    " | {}",
                    parameter
                        .last_modified_date
                        .fmt(aws_sdk_ssm::primitives::DateTimeFormat::HttpDate)
                        .unwrap()
                )?;
            }
            writeln!(
                f,
                " | {:value$} |",
                match parameter.r#type {
                    ParameterType::String => parameter.value.yellow(),
                    ParameterType::SecureString => parameter.value.red(),
                    _ => parameter.value.blue(),
                },
            )?;
        }

        Ok(())
    }
}
