# PSE

Parameter Store Editor

Well, more like Parameter Store Viewer at the time.
But, yeah, maybe one day.

## Usage

```
Usage: pse [OPTIONS] --profile <PROFILE> --region <REGION> <PATTERN>

Arguments:
  <PATTERN>

Options:
  -p, --profile <PROFILE>   [env: AWS_PROFILE=]
  -r, --region <REGION>     [env: AWS_REGION=]
  -d, --decrypt             Show decrypted secrets [env: WITH_DECRYPTION=]
  -s, --sort                Sort entries by path [env: SORT_BY_NAME=]
  -l, --last-modified-date  Show last modified date [env: LAST_MODIFIED_DATE=]
  -h, --help                Print help
```
